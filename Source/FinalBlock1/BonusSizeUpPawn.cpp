// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSizeUpPawn.h"
#include "PlayerPawn.h"

void ABonusSizeUpPawn::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ChangeSize(+0.5);
	}
}
