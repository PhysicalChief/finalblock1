// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractorInterface.h"
#include "KillZoneActor.generated.h"

class UBoxComponent;

UCLASS()
class FINALBLOCK1_API AKillZoneActor : public AActor, public IInteractorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKillZoneActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* killZone;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* killTarget, const FHitResult& hitResult) override;

};
