// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractorInterface.h"

// Add default functionality here for any IInteractorInterface functions that are not pure virtual.

void IInteractorInterface::Interact(const FHitResult& hitResult)
{
}

void IInteractorInterface::Interact(AActor* otherActor, const FHitResult& hitResult)
{
}

void IInteractorInterface::Interact(float score)
{
}

