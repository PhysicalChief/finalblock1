// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SaveScore.generated.h"


/**
 * 
 */
UCLASS()
class FINALBLOCK1_API USaveScore : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<int32> playerScore = TArray<int32>({ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		FString SaveSlotName;

	USaveScore();
};
