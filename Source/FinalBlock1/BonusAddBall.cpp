// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusAddBall.h"
#include "PlayerPawn.h"

void ABonusAddBall::GiveBonus(AActor* bonusTarget) 
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ballCounter++;
		pawn->AddBall();
	}
	return;
}
