// Fill out your copyright notice in the Description page of Project Settings.


#include "GameFieldACtor.h"
#include "BlockActor.h"
#include "MoveableBlockActor.h"
#include "Components/BoxComponent.h"

// Sets default values
AGameFieldACtor::AGameFieldACtor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	spawnField = CreateDefaultSubobject<UBoxComponent>(TEXT("spawndField"));
	spawnField->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

// Called when the game starts or when spawned
void AGameFieldACtor::BeginPlay()
{
	Super::BeginPlay();

	auto gameInstance = GetWorld()->GetGameInstance();

	auto castGameInstance = Cast<UCustomGameInstance>(gameInstance);
	if (IsValid(castGameInstance)) {
		auto newGameMode = castGameInstance->gameMode;

		switch (newGameMode)
		{
		case EGameMode::GameModeEasy:
			gameMode = EGameMode::GameModeEasy;
			break;
		case EGameMode::GameModeHard:
			gameMode = EGameMode::GameModeHard;
			break;
		default:
			break;
		}
	}
	switch (gameMode)
	{
	case EGameMode::GameModeEasy:
		switch (FMath::RandRange(0, 2))
		{
		case 0:
			SpawnXTypeFieldStatic();
			break;
		case 1:
			SpawnHorizontalStatic();
			break;
		case 2:
			SpawnVerticalStatic();
			break;
		default:
			break;
		}
		break;
	case EGameMode::GameModeHard:
		SpawnXTypeFieldDynamic();
		break;
	default:
		break;
	}


	//SpawnXTypeFieldDynamic();
	//SpawnHorizontalStatic();
	//SpawnVerticalStatic();
}

// Called every frame
void AGameFieldACtor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGameFieldACtor::SpawnXTypeFieldStatic()
{
	leftBorderPoint = spawnField->GetNavigationBounds().Min;
	rightBorderPoint = spawnField->GetNavigationBounds().Max;

	FVector* midPoint = new FVector();
	midPoint->X = (leftBorderPoint.X + rightBorderPoint.X) / 2;
	midPoint->Y = (leftBorderPoint.Y + rightBorderPoint.Y) / 2;
	midPoint->Z = (leftBorderPoint.Z + rightBorderPoint.Z) / 2;

	FVector* spawnedBlockPossition = new FVector();
	*spawnedBlockPossition = *midPoint;

	FTransform blockTransform = FTransform();
	blockTransform.SetLocation(*spawnedBlockPossition);
	blockTransform.SetScale3D(FVector(1, 1, 1));
	ABlockActor* newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
	blocksCount++;
	newBlock->fieldOwner = this;
	float lives = 1;

	while (spawnedBlockPossition->Y + blockOffsetY < rightBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y + blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}

	*spawnedBlockPossition = *midPoint;
	lives = 1;

	while (spawnedBlockPossition->Y - blockOffsetY > leftBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y - blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}

	*spawnedBlockPossition = *midPoint;
	lives = 1;

	while (spawnedBlockPossition->Y + blockOffsetY < rightBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y + blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) * -1 + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}

	*spawnedBlockPossition = *midPoint;
	lives = 1;

	while (spawnedBlockPossition->Y - blockOffsetY > leftBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y - blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) * -1 + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}

}

void AGameFieldACtor::SpawnXTypeFieldDynamic()
{
	leftBorderPoint = spawnField->GetNavigationBounds().Min;
	rightBorderPoint = spawnField->GetNavigationBounds().Max;

	FVector* midPoint = new FVector();
	midPoint->X = (leftBorderPoint.X + rightBorderPoint.X) / 2;
	midPoint->Y = (leftBorderPoint.Y + rightBorderPoint.Y) / 2;
	midPoint->Z = (leftBorderPoint.Z + rightBorderPoint.Z) / 2;

	FVector* spawnedBlockPossition = new FVector();
	*spawnedBlockPossition = *midPoint;

	FTransform blockTransform = FTransform();
	blockTransform.SetLocation(*spawnedBlockPossition);
	blockTransform.SetScale3D(FVector(1, 1, 1));
	AMoveableBlockActor* newBlock = GetWorld()->SpawnActor<AMoveableBlockActor>(blockActorClassMoveable, blockTransform);
	newBlock->fieldOwner = this;
	newBlock->leftPoint = *spawnedBlockPossition + FVector(0, -150, 0);
	newBlock->rightPoint = *spawnedBlockPossition + FVector(0, 150, 0);
	blocksCount++;

	float standartSpeed = newBlock->movementCoef;
	float speed = standartSpeed;
	float lives = 1;

	while (spawnedBlockPossition->Y + blockOffsetY < rightBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y + blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<AMoveableBlockActor>(blockActorClassMoveable, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		newBlock->leftPoint = *spawnedBlockPossition + FVector(0, -blockMovable, 0);
		newBlock->rightPoint = *spawnedBlockPossition;
		newBlock->movementCoef *= -1;
		blockMovable *= blockMoveableCoef;
		speed *= blockMoveableCoef;
		newBlock->movementCoef *= speed;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}
	blockMovable = 100;
	*spawnedBlockPossition = *midPoint;
	speed = standartSpeed;
	lives = 1;

	while (spawnedBlockPossition->Y - blockOffsetY > leftBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y - blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<AMoveableBlockActor>(blockActorClassMoveable, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		newBlock->rightPoint = *spawnedBlockPossition + FVector(0, blockMovable, 0);
		newBlock->leftPoint = *spawnedBlockPossition;
		blockMovable *= blockMoveableCoef;
		speed *= blockMoveableCoef;
		newBlock->movementCoef *= speed;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}
	blockMovable = 100;
	*spawnedBlockPossition = *midPoint;
	speed = standartSpeed;
	lives = 1;

	while (spawnedBlockPossition->Y + blockOffsetY < rightBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y + blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) * -1 + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<AMoveableBlockActor>(blockActorClassMoveable, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		newBlock->leftPoint = *spawnedBlockPossition + FVector(0, -blockMovable, 0);
		newBlock->rightPoint = *spawnedBlockPossition;
		newBlock->movementCoef *= -1;
		blockMovable *= blockMoveableCoef;
		speed *= blockMoveableCoef;
		newBlock->movementCoef *= speed;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}
	blockMovable = 100;
	*spawnedBlockPossition = *midPoint;
	speed = standartSpeed;
	lives = 1;

	while (spawnedBlockPossition->Y - blockOffsetY > leftBorderPoint.Y) {
		spawnedBlockPossition->Y = spawnedBlockPossition->Y - blockOffsetY;
		spawnedBlockPossition->X = ((spawnedBlockPossition->Y - midPoint->Y) * (rightBorderPoint.X - midPoint->X)) / (rightBorderPoint.Y - midPoint->Y) * -1 + midPoint->X;

		blockTransform.SetLocation(*spawnedBlockPossition);
		newBlock = GetWorld()->SpawnActor<AMoveableBlockActor>(blockActorClassMoveable, blockTransform);
		blocksCount++;
		newBlock->fieldOwner = this;
		newBlock->rightPoint = *spawnedBlockPossition + FVector(0, blockMovable, 0);
		newBlock->leftPoint = *spawnedBlockPossition;
		blockMovable *= blockMoveableCoef;
		speed *= blockMoveableCoef;
		newBlock->movementCoef *= speed;
		lives *= blockMoveableCoef;
		newBlock->lives = lives;
		newBlock->changeMaterial();
	}
}

void AGameFieldACtor::SpawnChestTypeField()
{
}

void AGameFieldACtor::SpawnHorizontalStatic()
{
	leftBorderPoint = spawnField->GetNavigationBounds().Min;
	rightBorderPoint = spawnField->GetNavigationBounds().Max;

	float y = (rightBorderPoint.Y - leftBorderPoint.Y) / ((rightBorderPoint.Y - leftBorderPoint.Y - blockOffsetY / 2) / blockOffsetY);

	FVector position = leftBorderPoint + FVector(blockOffsetX / 2, y / 2, (leftBorderPoint.Z + rightBorderPoint.Z / 2));

	FTransform blockTransform = FTransform();
	
	blockTransform.SetScale3D(FVector(1, 1, 1));
	for (int j = 0; j < (rightBorderPoint.X - leftBorderPoint.X - blockOffsetX / 2) / blockOffsetX / 2; j++) {
		for (int i = 0; i < (rightBorderPoint.Y - leftBorderPoint.Y - blockOffsetY / 2) / blockOffsetY - 1; i++) {
			blockTransform.SetLocation(position);
			ABlockActor* newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
			newBlock->fieldOwner = this;
			newBlock->lives = j + 1;
			newBlock->changeMaterial();
			position += FVector(0, y, 0);
			blocksCount++;
		}
		position = leftBorderPoint + FVector(2 * blockOffsetX * (j + 1), y / 2, (leftBorderPoint.Z + rightBorderPoint.Z / 2));
	}
}

void AGameFieldACtor::SpawnVerticalStatic()
{
	leftBorderPoint = spawnField->GetNavigationBounds().Min;
	rightBorderPoint = spawnField->GetNavigationBounds().Max;

	float x = (rightBorderPoint.X - leftBorderPoint.X) / ((rightBorderPoint.X - leftBorderPoint.X - blockOffsetX / 2) / blockOffsetX);
	

	FVector position = leftBorderPoint + FVector(x / 2, blockOffsetY / 2, (leftBorderPoint.Z + rightBorderPoint.Z / 2));

	FTransform blockTransform = FTransform();
	blockTransform.SetScale3D(FVector(1, 1, 1));
	for (int i = 0; i < (rightBorderPoint.Y - leftBorderPoint.Y - blockOffsetY / 2) / blockOffsetY / 2; i++) {
		for (int j = 0; j < (rightBorderPoint.X - leftBorderPoint.X - blockOffsetX / 2) / blockOffsetX - 1; j++) {
			blockTransform.SetLocation(position);
			ABlockActor* newBlock = GetWorld()->SpawnActor<ABlockActor>(blockActorClass, blockTransform);
			newBlock->fieldOwner = this;
			newBlock->lives = j + 1;
			newBlock->changeMaterial();
			position += FVector(x, 0, 0);
			blocksCount++;
		}
		position = leftBorderPoint + FVector(x / 2, 2 * blockOffsetY * (i + 1), (leftBorderPoint.Z + rightBorderPoint.Z / 2));
	}
}

void AGameFieldACtor::CheckBlocksCount()
{
	if (blocksCount == 0) {
		ShowEndGame();
	}
}

