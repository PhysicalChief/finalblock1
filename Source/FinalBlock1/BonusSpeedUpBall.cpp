// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedUpBall.h"
#include "PlayerPawn.h"

void ABonusSpeedUpBall::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ChangeSpeed(+0.5);
	}
}
