// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractorInterface.h"
#include "BallActor.generated.h"


class UStaticMeshComponent;
class APlayerPawn;
class ABlowupActor;
class UParticleSystem;


UENUM()
enum class EBallState {
	STANDART, 
	IGNOREONE,
	IGNORETIME,
	BLOWUP
};

UCLASS()
class FINALBLOCK1_API ABallActor : public AActor, public IInteractorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABallActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int indexBall;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		FVector movementDiraction;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float speedBall = 50;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* ballMeshComponent;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* standrtMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* ignoreMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* blowupMaterial;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		EBallState ballState = EBallState::STANDART;

	UPROPERTY(BlueprintReadOnly)
		APlayerPawn* pawnOwner;


	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlowupActor> blowupActorClass;


	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* contactPS;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* blowupPS;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* ignorePS;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ChangeMaterial(int effectType);

	EBallState currentBallState = EBallState::STANDART;

	int ignoreValue;

	FHitResult* hitResult = new FHitResult();

	virtual void Interact(AActor* otherActor, const FHitResult& hitResult) override;
	virtual void Interact(float score) override;

	void blowUp();

	float calculateRotateAngel(const FHitResult& hit);
};
