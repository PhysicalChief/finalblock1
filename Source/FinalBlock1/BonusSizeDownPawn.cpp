// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSizeDownPawn.h"
#include "PlayerPawn.h"

void ABonusSizeDownPawn::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ChangeSize(-0.2);
	}
}
