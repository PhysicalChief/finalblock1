// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CustomGameInstance.h"
#include "GameFieldACtor.generated.h"

class UBoxComponent;
class ABlockActor;
class ABonusBasic;
class AMoveableBlockActor;

UCLASS()
class FINALBLOCK1_API AGameFieldACtor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameFieldACtor();

	UPROPERTY(BlueprintReadWrite)
		EFieldType fieldType;


	UPROPERTY(BlueprintReadWrite)
		EGameMode gameMode = EGameMode::GameModeEasy;

	UPROPERTY(EditDefaultsOnly)
		FVector leftBorderPoint;

	UPROPERTY(EditDefaultsOnly)
		FVector rightBorderPoint;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* spawnField;

	UPROPERTY(BlueprintReadWrite)
		TArray<ABlockActor*> fieldBlocks;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlockActor> blockActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlockActor> blockActorClassMoveable;


	UPROPERTY(EditDefaultsOnly)
		float blockMovable = 100;
	UPROPERTY(EditDefaultsOnly)
		float blockMoveableCoef = 1;

	UPROPERTY(EditDefaultsOnly)
		float blockOffsetY = 150;
	
	UPROPERTY(EditDefaultsOnly)
		float blockOffsetX = 100;

	UPROPERTY(EditDefaultsOnly)
		TMap<int, TSubclassOf<ABonusBasic>> mapBonusClass;

	UPROPERTY(BlueprintReadOnly)
		int blocksCount = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void SpawnXTypeFieldStatic();

	UFUNCTION(BlueprintCallable)
		void SpawnXTypeFieldDynamic();

	UFUNCTION(BlueprintCallable)
		void SpawnChestTypeField();

	UFUNCTION(BlueprintCallable)
		void SpawnHorizontalStatic();

	UFUNCTION(BlueprintCallable)
		void SpawnVerticalStatic();

	UFUNCTION(BlueprintImplementableEvent)
		void ShowEndGame();

	UFUNCTION(BlueprintCallable)
		void CheckBlocksCount();
};
