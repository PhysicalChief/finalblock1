// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractorInterface.h"
#include "GameFramework/Actor.h"
#include "BlockActor.generated.h"

class UStaticMeshComponent;
class IBonusInterface;
class AGameFieldACtor;

UENUM()
enum EBonusType {
	SPEEDUPBALL,
	SPEEDDOWNBALL,
	SPEEDUPPAWN,
	SPEEDDOWNPAWN,
	SIZEUPPAWN,
	SIZEDOWNPAWN,
	IGNOREBLOCK,
	INGNOREBLOCKTIME,
	BLOWUP,
	ADDBALL
};

UCLASS()
class FINALBLOCK1_API ABlockActor : public AActor, public IInteractorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* blockMeshComponent;

	UPROPERTY(EditDefaultsOnly)
		int bonusChance = 15;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		AGameFieldACtor* fieldOwner;

	UPROPERTY(EditDefaultsOnly)
		int32 blockScore = 100;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int lives = 1;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);

	UFUNCTION(BlueprintImplementableEvent)
		void changeMaterial();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* otherActor, const FHitResult& hitResult) override;

};
