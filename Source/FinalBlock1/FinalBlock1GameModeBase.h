// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FinalBlock1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FINALBLOCK1_API AFinalBlock1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
