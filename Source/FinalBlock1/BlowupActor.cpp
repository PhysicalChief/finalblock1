// Fill out your copyright notice in the Description page of Project Settings.


#include "BlowupActor.h"
#include "Components/SphereComponent.h"
#include "InteractorInterface.h"

// Sets default values
ABlowupActor::ABlowupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	blowUpSphere = CreateDefaultSubobject<USphereComponent>(TEXT("blowUpSphere"));
	blowUpSphere->bDrawOnlyIfSelected = true;
	blowUpSphere->SetSphereRadius(blowupRadius);
	blowUpSphere->SetWorldLocation(GetActorLocation());
	blowUpSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	blowUpSphere->SetCollisionResponseToAllChannels(ECR_Overlap);
	blowUpSphere->OnComponentBeginOverlap.AddDynamic(this, &ABlowupActor::HandleBeginOverlap);
}

// Called when the game starts or when spawned
void ABlowupActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABlowupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlowupActor::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		//GEngine->AddOnScreenDebugMessage(-1, 300.0f, FColor::Yellow, otherActor->GetActorLabel());
		reflection->Interact(this, sweepResult);
	}
}