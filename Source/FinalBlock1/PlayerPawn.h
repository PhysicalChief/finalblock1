// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InteractorInterface.h"
#include "BonusInterface.h"
#include "PlayerPawn.generated.h"

class UStaticMeshComponent;
class UCapsuleComponent;
class ABallActor;
class UParticleSystemComponent;

USTRUCT()
struct FActorSaveData
{
	GENERATED_BODY()

public:
	UPROPERTY()
		TArray<int32> ByteData;
};

UCLASS()

class FINALBLOCK1_API APlayerPawn : public APawn, public IInteractorInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void HandlerPlayerPawnMoveInpput(float value);


	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void HandlerPlayerPushBallInpput();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* platformMeshComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UParticleSystemComponent* leftPS;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UParticleSystemComponent* rightPS;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UCapsuleComponent* ballsSpawnCapsule;

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UPawnMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		TArray<ABallActor*> ballsArray;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		ABallActor* ballForSpawn;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int32 playerScore = 0;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABallActor> ballActorClass;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ChangeLivesVisible();

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
								AActor* otherActor,
								UPrimitiveComponent* otherComponent,
								int32 otherBodyIndex,
								bool bFromSweep,
								const FHitResult& sweepResult);

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float movemntCoef = 2;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int ballCounter = 1;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int lives = 3;

	bool placedSpawnBall = true;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		float ballsSpeed = 7;

	float pawnScale = 1;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void BackToMenu();

	UFUNCTION(BlueprintCallable)
		void saveGameResult();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateFieldActor();

	void AddBall();

	void ChangeSpeed(float speedCoef);

	void ChangeSize(float sizeCoef);

	void ChangeBallsSpeed(float speedCoef);

	void AddBlowUpBall();

	void AddIgnoreBall(int count = 1);

	void AddIngnoreTimeBall(int count = 100);

	void CheckLives();
};
