// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BonusInterface.h"
#include "BonusBasic.generated.h"

UCLASS()
class FINALBLOCK1_API ABonusBasic : public AActor, public IBonusInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBasic();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		FVector movementDiraction = FVector(-1, 0, 0);

	UPROPERTY(EditDefaultsOnly)
		float speedBall = 4;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* ballMeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GiveBonus(AActor* bonusTarget);

};
