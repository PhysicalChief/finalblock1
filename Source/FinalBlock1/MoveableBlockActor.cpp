// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveableBlockActor.h"
#include <Math/UnrealMathUtility.h>

void AMoveableBlockActor::Tick(float DeltaTime)
{
	FVector currnetPosition = GetActorLocation();
	FHitResult* hitResult = new FHitResult;
	AddActorWorldOffset(FVector(0, 1 * movementCoef, 0), true, hitResult);
	FVector actorLocation = GetActorLocation();
	if (actorLocation.Y >= rightPoint.Y || actorLocation.Y <= leftPoint.Y) {
		movementCoef *= -1;
	}
}

void AMoveableBlockActor::BeginPlay()
{
	Super::BeginPlay();
}
