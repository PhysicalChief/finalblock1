// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlowupActor.generated.h"

class USphereComponent;

UCLASS()
class FINALBLOCK1_API ABlowupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlowupActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		USphereComponent* blowUpSphere;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	UPROPERTY(EditDefaultsOnly)
		float blowupRadius = 200;
	
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		int busyStatus = 0;
};
