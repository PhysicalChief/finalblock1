// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBasic.h"
#include "BonusBlowUp.generated.h"

/**
 * 
 */
UCLASS()
class FINALBLOCK1_API ABonusBlowUp : public ABonusBasic
{
	GENERATED_BODY()
	
public:
	virtual void GiveBonus(AActor* bonusTarget) override;
};
