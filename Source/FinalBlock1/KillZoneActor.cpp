// Fill out your copyright notice in the Description page of Project Settings.


#include "KillZoneActor.h"
#include "BallActor.h"
#include "BonusBasic.h"
#include "PlayerPawn.h"
#include "Components/BoxComponent.h"

// Sets default values
AKillZoneActor::AKillZoneActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	killZone = CreateDefaultSubobject<UBoxComponent>(TEXT("killZone"));
	killZone->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	killZone->SetCollisionResponseToAllChannels(ECR_Overlap);
	killZone->OnComponentBeginOverlap.AddDynamic(this, &AKillZoneActor::HandleBeginOverlap);
}

void AKillZoneActor::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	ABallActor* ball = Cast<ABallActor>(otherActor);
	if (ball) {
		auto ballOwner = ball->pawnOwner;
		ballOwner->ballsArray.Remove(ball);
		ball->Destroy();
		return;
	}
	otherActor->Destroy();
	return;
}

// Called when the game starts or when spawned
void AKillZoneActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillZoneActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKillZoneActor::Interact(AActor* killTarget, const FHitResult& hitResult)
{
	ABallActor* ball = Cast<ABallActor>(killTarget);
	if (ball) {
		auto ballOwner = ball->pawnOwner;
		ballOwner->ballCounter--;
		ballOwner->ballsArray.Remove(ball);
		ballOwner->CheckLives();
		ball->Destroy();
		return;
	}
	killTarget->Destroy();
	return;
}

