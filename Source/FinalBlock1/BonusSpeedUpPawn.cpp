// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedUpPawn.h"
#include "PlayerPawn.h"

void ABonusSpeedUpPawn::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ChangeSpeed(+0.5);
	}
}
