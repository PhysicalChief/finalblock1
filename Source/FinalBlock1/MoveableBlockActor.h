// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlockActor.h"
#include "MoveableBlockActor.generated.h"

/**
 * 
 */
UCLASS()
class FINALBLOCK1_API AMoveableBlockActor : public ABlockActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
		float movementCoef = 1;


	FVector leftPoint;
	FVector rightPoint;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
