// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WallActor.generated.h"

UCLASS()
class FINALBLOCK1_API AWallActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* wallMeshComponent;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
								AActor* otherActor,
								UPrimitiveComponent* otherComponent,
								int32 otherBodyIndex,
								bool bFromSweep,
								const FHitResult& sweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
