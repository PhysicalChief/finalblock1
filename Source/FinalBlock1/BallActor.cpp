// Fill out your copyright notice in the Description page of Project Settings.


#include "BallActor.h"
#include "PlayerPawn.h"
#include "BlowupActor.h"
#include "BlockActor.h"

#include <Engine/Classes/Components/StaticMeshComponent.h>
#include <GameFramework/KillZVolume.h>
#include <Particles/ParticleSystem.h>
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ABallActor::ABallActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ballMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ballMeshComponent"));
	ballMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ballMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	ballMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABallActor::HandleBeginOverlap);

	contactPS = CreateDefaultSubobject<UParticleSystem>(TEXT("contactPS"));
	blowupPS = CreateDefaultSubobject<UParticleSystem>(TEXT("blowupPS"));
	ignorePS = CreateDefaultSubobject<UParticleSystem>(TEXT("ignorePS"));
}

void ABallActor::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(this, sweepResult);
	}
}

// Called when the game starts or when spawned
void ABallActor::BeginPlay()
{
	Super::BeginPlay();
	FVector currentPosition = GetActorLocation();
	movementDiraction.Y = FMath::RandRange(-5, 5);
	movementDiraction.X = 5;
	movementDiraction.Z = 0;

	movementDiraction.Normalize();
}

// Called every frame
void ABallActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
 	AddActorWorldOffset(movementDiraction * speedBall, true, hitResult);
	if (ballState == EBallState::IGNORETIME) {
		if (ignoreValue > 0) {
			currentBallState = EBallState::STANDART;
		}
		else {
			ignoreValue--;
		}
	}
}

void ABallActor::Interact(AActor* otherActor, const FHitResult& hit)
{
	ABallActor* ball = Cast <ABallActor>(otherActor);
	if (ball) return;
	ABlowupActor* blowup = Cast<ABlowupActor>(otherActor);
	if (blowup) return;
	
	ABlockActor* tergetBlock = Cast<ABlockActor>(otherActor);
	if (tergetBlock) {
		FRotator rt;
		if (hit.ImpactNormal == FVector(1, 0, 0)) {
			rt = FRotator(0, 90, -90);
		}
		else if (hit.ImpactNormal == FVector(-1, 0, 0)) {
			rt = FRotator(0, 90, 90);
		}
		else if (hit.ImpactNormal == FVector(0, 1, 0)) {
			rt = FRotator(0, 0, 90);
		}
		else if (hit.ImpactNormal == FVector(0, -1, 0)) {
			rt = FRotator(180, 0, -90);
		}
		FTransform tf = FTransform(rt);
		tf.SetLocation(hit.ImpactPoint);

		switch (ballState)
		{
		case EBallState::STANDART:
			movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);

			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				contactPS,
				tf
			);
			break;
		case EBallState::IGNOREONE:
			if (--ignoreValue == 0) {
				ballState = EBallState::STANDART;
				ballMeshComponent->SetMaterial(0, standrtMaterial);
			}

			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				ignorePS,
				tf
			);
			break;
		case EBallState::IGNORETIME:
			if (ignoreValue == 0) {
				ballState = EBallState::STANDART;
				ballMeshComponent->SetMaterial(0, standrtMaterial);
			}
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				ignorePS,
				tf
			);
			break;
		case EBallState::BLOWUP:
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				blowupPS,
				hit.ImpactPoint,
				FRotator(0, 0, 90)
			);
			ballMeshComponent->SetMaterial(0, standrtMaterial);
			movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
			ballState = EBallState::STANDART;
			blowUp();
			break;
		default:
			movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
			break;
		}
		return;
	}

	movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
}

void ABallActor::Interact(float score)
{
	pawnOwner->playerScore += score;
}

void ABallActor::blowUp()
{
	FTransform transform = this->GetTransform();
	ABlowupActor* blowup = GetWorld()->SpawnActor<ABlowupActor>(blowupActorClass, transform);
	if (blowup)
		blowup->Destroy();
}

float ABallActor::calculateRotateAngel(const FHitResult& hit)
{
	float resultAngleInRadians = 0.0f;
	FVector copyHit = hit.ImpactNormal;
	copyHit.Normalize();
	FVector move = movementDiraction;
	move.Normalize();

	auto crossProduct = FVector::CrossProduct(move, copyHit);
	auto dotProduct = FVector::DotProduct(move, copyHit);

	if (crossProduct.Z > 0)
	{
		resultAngleInRadians = acosf(dotProduct);
	}
	else
	{
		resultAngleInRadians = -1 * acosf(dotProduct);
	}

	auto resultAngleInDegrees = FMath::RadiansToDegrees(resultAngleInRadians);
	return resultAngleInDegrees;
}
