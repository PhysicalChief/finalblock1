// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include <Engine/Classes/Components/CapsuleComponent.h>
#include <Particles/ParticleSystemComponent.h>
#include <GameFramework/FloatingPawnMovement.h>
#include "BallActor.h"
#include "SaveScore.h"
#include "BonusInterface.h"
#include <Kismet/GameplayStatics.h>


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	platformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("platformMeshComponent"));
	platformMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	platformMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	platformMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &APlayerPawn::HandleBeginOverlap);

	leftPS = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("leftPS"));
	leftPS->AttachTo(platformMeshComponent);
	rightPS = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("rightPS"));
	rightPS->AttachTo(platformMeshComponent);
	
	ballsSpawnCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ballsSpawnCapsule"));
	ballsSpawnCapsule->AttachTo(platformMeshComponent);

	MovementComponent = CreateDefaultSubobject<UPawnMovementComponent, UFloatingPawnMovement>(TEXT("movementComponentName"));
	MovementComponent->UpdatedComponent = platformMeshComponent;

	ballsArray = TArray<ABallActor*>();
}

void APlayerPawn::HandlerPlayerPawnMoveInpput(float value)
{
	

	if (value != 0.f)
	{
		if (Controller)
		{
			FRotator const ControlSpaceRot = Controller->GetControlRotation();

			// transform to world space and add it
			AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::Y), value * movemntCoef);
			if (value > 0) rightPS->Activate();
			else leftPS->Activate();
		}
	}
	else {
		leftPS->Deactivate();
		rightPS->Deactivate();
	}
}

void APlayerPawn::HandlerPlayerPushBallInpput()
{
	if (ballForSpawn != NULL) {
		ballForSpawn->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		ballForSpawn->indexBall = ballsArray.Add(ballForSpawn);
		ballForSpawn->pawnOwner = this;
		ballForSpawn->speedBall = ballsSpeed;
		if (ballCounter > ballsArray.Num()) {
			AddBall();
		}
	}
}



void APlayerPawn::HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
									AActor* otherActor,
									UPrimitiveComponent* otherComponent,
									int32 otherBodyIndex,
									bool bFromSweep,
									const FHitResult& sweepResult)
{
	IBonusInterface* bonus = Cast<IBonusInterface>(otherActor);
	if (bonus) {
		bonus->GiveBonus(this);
		otherActor->Destroy();
		return;
	}
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(this, sweepResult);
		return;
	}
}

void APlayerPawn::saveGameResult()
{
	bool SaveExists = UGameplayStatics::DoesSaveGameExist("ScoreSlot", 0);
	if (!SaveExists) {
		if (USaveScore* newSave = Cast<USaveScore>(UGameplayStatics::CreateSaveGameObject(USaveScore::StaticClass()))) {
			newSave->SaveSlotName = TEXT("ScoreSlot");
			newSave->playerScore.Add(playerScore);
			if (UGameplayStatics::SaveGameToSlot(newSave, "ScoreSlot", 0)) {

			}
		}
	}
	if (USaveScore* loadedScore = Cast<USaveScore>(UGameplayStatics::LoadGameFromSlot("ScoreSlot", 0))) {
		loadedScore->playerScore.Add(playerScore);
		loadedScore->playerScore.Sort([](const int32& ip1, const int32& ip2) {
			return  ip1 > ip2;
			});
		if (loadedScore->playerScore.Num() > 10) loadedScore->playerScore.Pop();
		if (UGameplayStatics::SaveGameToSlot(loadedScore, "ScoreSlot", 0)) {

		}
	}
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	AddBall();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("PawnMove", this, &APlayerPawn::HandlerPlayerPawnMoveInpput);
	PlayerInputComponent->BindAction("PushBall", IE_Pressed, this, &APlayerPawn::HandlerPlayerPushBallInpput);
}

void APlayerPawn::CreateFieldActor()
{
}

void APlayerPawn::AddBall()
{
	if (ballCounter - 1 == ballsArray.Num()) {
		FTransform ballTransfrom = ballsSpawnCapsule->GetComponentTransform();
		ballTransfrom.SetScale3D(FVector(1, 1, 1));
		ballForSpawn = GetWorld()->SpawnActor<ABallActor>(ballActorClass, ballTransfrom);
		ballForSpawn->AttachToComponent(ballsSpawnCapsule, FAttachmentTransformRules::KeepWorldTransform);
		ballForSpawn->speedBall = 0;
		placedSpawnBall = true;
	}
}

void APlayerPawn::ChangeSpeed(float speedCoef)
{
	movemntCoef += speedCoef;
}

void APlayerPawn::ChangeSize(float sizeCoef)
{
	if (pawnScale + sizeCoef > 5 && pawnScale + sizeCoef < 0.5)
		return;
	pawnScale += sizeCoef;
	platformMeshComponent->SetWorldScale3D(FVector(0.25, pawnScale, 1));
}

void APlayerPawn::ChangeBallsSpeed(float speedCoef)
{
	if (ballsSpeed + movemntCoef < 2)
		return;
	ballsSpeed += speedCoef;
	for (auto ball = ballsArray.CreateIterator(); ball; ball++) {
		(*ball)->speedBall = ballsSpeed;
	}
}

void APlayerPawn::AddBlowUpBall()
{
	if (ballsArray.Num() > 0) {
		int n = FMath::RandRange(0, ballsArray.Num() - 1);
		ballsArray[n]->ballState = EBallState::BLOWUP;
		ballsArray[n]->ballMeshComponent->SetMaterial(0, ballsArray[n]->blowupMaterial);
	}
	else if (ballForSpawn != NULL) {
		ballForSpawn->ballState = EBallState::BLOWUP;
		ballForSpawn->ballMeshComponent->SetMaterial(0, ballForSpawn->ignoreMaterial);
	}
}

void APlayerPawn::AddIgnoreBall(int count)
{
	if (ballsArray.Num() > 0) {
		int n = FMath::RandRange(0, ballsArray.Num() - 1);
		ballsArray[n]->ballState = EBallState::IGNOREONE;
		ballsArray[n]->ignoreValue = count;
		ballsArray[n]->ballMeshComponent->SetMaterial(0, ballsArray[n]->ignoreMaterial);
	}
	else if (ballForSpawn != NULL) {
		ballForSpawn->ballState = EBallState::IGNOREONE;
		ballForSpawn->ignoreValue = count;
		ballForSpawn->ballMeshComponent->SetMaterial(0, ballForSpawn->ignoreMaterial);
	}
}

void APlayerPawn::AddIngnoreTimeBall(int count)
{
	if (ballsArray.Num() > 0) {
		int n = FMath::RandRange(0, ballsArray.Num() - 1);
		ballsArray[n]->ballState = EBallState::IGNORETIME;
		ballsArray[n]->ignoreValue = count;
		ballsArray[n]->ballMeshComponent->SetMaterial(0, ballsArray[n]->ignoreMaterial);
	}
	else if (ballForSpawn != NULL) {
		ballForSpawn->ballState = EBallState::IGNORETIME;
		ballForSpawn->ignoreValue = count;
		ballForSpawn->ballMeshComponent->SetMaterial(0, ballForSpawn->ignoreMaterial);
	}
}

void APlayerPawn::CheckLives()
{
	if (ballCounter != 0) return;
	else {
		if (lives == 1) {
			saveGameResult();
			BackToMenu();
		}
		else {
			lives--;
			ballCounter++;
			AddBall();
			ChangeLivesVisible();
		}
	}
}


