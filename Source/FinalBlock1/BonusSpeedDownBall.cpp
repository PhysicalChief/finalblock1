// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeedDownBall.h"
#include "PlayerPawn.h"

void ABonusSpeedDownBall::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->ChangeBallsSpeed(-0.5);
	}
}
