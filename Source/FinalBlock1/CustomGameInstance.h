// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CustomGameInstance.generated.h"

UENUM(BlueprintType)
enum class EFieldType : uint8 {
	CHEST_TYPE,
	EX_TYPE
};

UENUM(BlueprintType)
enum class EGameMode : uint8 {
	GameModeEasy,
	GameModeHard
};

/**
 * 
 */
UCLASS()
class FINALBLOCK1_API UCustomGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		EFieldType fieldType;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		EGameMode gameMode;
};
